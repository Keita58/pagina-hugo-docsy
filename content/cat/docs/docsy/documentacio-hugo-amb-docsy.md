---
title: "Documentació Hugo Amb Docsy"
description: "Documentació per a l'assignatura de Sistemes"
date: 2024-05-22T19:12:43+02:00
weight: 1
---

## Preliminars

Per a que la nostra pàgina funcioni correctament hem d'instal·lar diverses coses.
Primer de tot, hem d'instal·lar [hugo](https://github.com/gohugoio/hugo/releases) i [go](https://go.dev/dl/) com a programes externs.

## Clonar el repositori d'exemple

Una vegada hem instal·lat els programes anteriorment esmenats, podem clonar el repositori d'exemple que se'ns facilita des de la pàgina oficial de [docsy](https://docsy.dev). 
Una vegada clonat el repositori podem descarregar uns paquets de npm per a que ens funcioni el CSS, que son: "npm install -D autoprefixer", "npm install -D postcss-cli" i "npm install -D postcss". Per a comprovar que tot funciona correctament, podem escriure "hugo server" per a que se'ns obri la pàgina en el nostre entorn local.

## Pujar la pàgina al GitLab

Ara que sabem que la pàgina ens funciona en local ho podem pujar al Gitlab per a tenir la pàgina web sempre disponible. Per a que la pàgina web funcioni correctament haurem de crear un arxiu anomenat .gitlab-ci.yml, el qual podem extreure de la següent pàgina web: [pàgina](https://gitlab.com/pages/hugo/-/blob/main/.gitlab-ci.yml).
Pugem el projecte al repositori i després d'esperar un minut aproximadament la nostra pàgina estarà creada.